from multiprocessing import cpu_count
PROCESS_FORMATS = ["simple","undersample"]

configfile: "config.yaml"

rule all:
    input:
        "data/reports/lama",
        "data/reports/catboost"

rule train:
    input:
        expand("data/preprocess/data_{process_format}.csv", process_format=PROCESS_FORMATS)
    output:
        directory("data/reports/lama"),
        directory("data/reports/catboost")
    shell:
        "python3 src/model_training/lama_train.py && python3 src/model_training/cat_train.py"


rule preprocess_dataset:
    input:
        "data/raw/2015-street-tree-census-tree-data.csv"
    output:
        expand("data/preprocess/data_{process_format}.csv", process_format=PROCESS_FORMATS)
    shell:
        "python3 src/data_wranglers/prepare_dataset_simple.py && python3 src/data_wranglers/prepare_dataset_undersample.py"


rule download_dataset:
    output:
        "ny-2015-street-tree-census-tree-data.zip"
    shell:
        "kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data"

rule unzip_dataset:
    input:
        "ny-2015-street-tree-census-tree-data.zip"
    output:
        "data/raw/2015-street-tree-census-tree-data.csv"
    threads: cpu_count()//2
    shell:
        "unzip {input} -d data/raw/ && rm {input}"
