from data_samplers import get_balanced_split

import pandas as pd
import os

RAW_DATASET = './data/raw/2015-street-tree-census-tree-data.csv'
data = pd.read_csv(RAW_DATASET).dropna()
data = data.rename(
    columns={
        "community board": "community_board",
        "council district": "council_district",
        "census tract": "census_tract",
    }
)

data.created_at = data.created_at.astype("datetime64[ns]")

indexes, _ = get_balanced_split(data.health,
                                trainSize=1,
                                getTestIndexes=False)
data = data.iloc[indexes]

OUT_DATASET_DIR = './data/preprocess'
if os.path.exists(OUT_DATASET_DIR):
    pass
else:
    os.mkdir(OUT_DATASET_DIR)
data.to_csv(OUT_DATASET_DIR + '/data_undersample.csv', index=False)
