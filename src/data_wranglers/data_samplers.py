import pandas as pd
import numpy as np


def get_balanced_split(target: pd.Series,
                       trainSize: float = 0.8,
                       getTestIndexes: bool = True,
                       seed: int = 42) -> tuple[list | None, list[int] | None]:
    """
    Function to get a safe and balanced split of the data for training and testing

    Args:
    - target: pd.Series, the target data for splitting
    - trainSize: float, the proportion of data to be used for training (default is 0.8)
    - getTestIndexes: bool, whether to return indexes for testing data (default is True)
    - shuffle: bool, whether to shuffle the indexes (default is False)
    - seed: bool, the random seed for shuffling the indexes (default is None)

    Returns:
    - tuple[list | None, list[int] | None]: indexes for training and testing data
    """
    # Get unique classes and their counts from the target data
    classes, counts = np.unique(target, return_counts=True)

    # Calculate the number of samples per class for the training data
    nPerClass = float(len(target))*float(trainSize)/float(len(classes))

    # Check if there is enough data to produce a balanced split,
    # if not, adjust the trainSize
    if nPerClass > np.min(counts):
        print("Insufficient data to produce a balanced training data split.")
        print(f"Classes found {classes}")
        print(f"Classes count {counts}")
        ts = float(trainSize*np.min(counts)*len(classes)) / float(len(target))
        print(f"trainSize is reset from {trainSize} to {ts}")
        trainSize = ts
        nPerClass = float(len(target))*float(trainSize)/float(len(classes))

    # Convert nPerClass to an integer
    nPerClass = int(nPerClass)
    print(f"Data splitting on {len(classes)} classes",
          f"and returning {nPerClass} per class")

    # Get indexes for the training data
    trainIndexes: list = []
    for c in classes:
        # Set random seed if provided
        if seed is not None:
            np.random.seed(seed)
        # Get indexes of samples belonging to the current class
        cIdxs = np.where(target == c)[0]
        # Randomly select nPerClass samples from the current class without replacement
        cIdxs = np.random.choice(cIdxs, nPerClass, replace=False)
        trainIndexes.extend(cIdxs)

    # Get indexes for the testing data if required
    testIndexes = None
    if getTestIndexes:
        testIndexes = list(set(range(len(target))) - set(trainIndexes))

    # Return the indexes for training and testing data
    return trainIndexes, testIndexes
