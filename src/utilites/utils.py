import os


def count_threads() -> int:
    """
    Determine the number of CPU threads available for parallel processing.

    Returns:
        int: The number of CPU threads available, leaving one core for system processes.
                If the CPU count is not available, returns 0.
    """
    all_cpu = os.cpu_count()
    if all_cpu is not None:
        return all_cpu - 1  # left one core for system
    else:
        return 0  # handle the case where cpu count is not available
