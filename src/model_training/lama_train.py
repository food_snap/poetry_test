# Standard python libraries
from IPython.display import display

# Essential DS libraries
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import torch

# LightAutoML presets, task and report generation
from lightautoml.automl.presets.tabular_presets import TabularAutoML
from lightautoml.tasks import Task
from lightautoml.report.report_deco import ReportDeco

# Self-made modules
from src.utilites.utils import count_threads

# Configs

DATASET_FILE = "./data/preprocess/data_simple.csv"
TARGET_NAME = "health"
TEST_SIZE = 0.2
TIMEOUT = 30 * 60  # value in seconds, multiply on 60 for minutes

RANDOM_STATE = 42
N_THREADS = count_threads()
N_FOLDS = 10

np.random.seed(RANDOM_STATE)
torch.set_num_threads(N_THREADS)

data = pd.read_csv(DATASET_FILE)

display(data.info())

train_data, test_data = train_test_split(
    data, test_size=TEST_SIZE, stratify=data[TARGET_NAME], random_state=RANDOM_STATE
)

# specify task type
task = Task("multiclass")

# specify feature roles
roles = {
    "target": TARGET_NAME,  # required
    "drop": ["tree_id"],  # remove tree's id
}

RD = ReportDeco(output_path="./data/reports/lama")

automl_rd = RD(
    TabularAutoML(
        task=task,
        timeout=TIMEOUT,
        cpu_limit=N_THREADS,
        reader_params={
            "n_jobs": N_THREADS,
            "cv": N_FOLDS,
            "random_state": RANDOM_STATE,
        },
    )
)

out_of_fold_predictions = automl_rd.fit_predict(train_data, roles=roles, verbose=1)

# class mapping
mapping = automl_rd.model.reader.class_mapping


def map_class(x):
    return mapping[x]


mapped = np.vectorize(map_class)
mapped(train_data["health"].values)
