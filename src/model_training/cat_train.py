import pandas as pd
from catboost import CatBoostClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score


DATASET_FILE = './data/preprocess/data_undersample.csv'
OUTPUT_PATH = 'data/reports/catboost'


data = pd.read_csv(DATASET_FILE)
data.drop(['tree_id', 'status', 'address', 'state'], axis=1, inplace=True)
data.council_district = data.council_district.astype(int)
data.census_tract = data.census_tract.astype(int)
data.created_at = data.created_at.astype('datetime64').astype(int).astype(float)
category_columns = [
    'curb_loc',
    'spc_latin',
    'spc_common',
    'steward',
    'guards',
    'sidewalk',
    'user_type',
    'problems',
    'postcode',
    'zip_city',
    'community_board',
    'borocode',
    'borough',
    'cncldist',
    'st_assem',
    'st_senate',
    'nta',
    'nta_name',
    'boro_ct',
    'council_district',
    'census_tract',
    'root_stone',
    'root_grate',
    'root_other',
    'trunk_wire',
    'trnk_light',
    'trnk_other',
    'brch_light',
    'brch_shoe',
    'brch_other'
    ]
for col in category_columns:
    data[col] = data[col].astype('category')

# Split the data into features and target
X = data.drop('health', axis=1)
y = data['health']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    random_state=42)

# Initialize the CatBoost model
model = CatBoostClassifier(iterations=100,
                           cat_features=category_columns,
                           learning_rate=0.1,
                           depth=6,
                           loss_function='MultiClass',
                           train_dir=OUTPUT_PATH
                           )

# Train the model
model.fit(X_train, y_train, eval_set=(X_test, y_test))

# Make predictions
predictions = model.predict(X_test)


f1_score = f1_score(y_test,
                    predictions,
                    average="micro")

print(f'f1 score: {f1_score}')
