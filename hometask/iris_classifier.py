import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


# Load Iris dataset and return dataframe with feature names
def load_data() -> tuple[pd.DataFrame, list]:
    """Load Iris dataset

    :return: dataframe with Iris dataset & feature_names list
    """
    data = load_iris()
    feature_names = data.feature_names
    df = pd.DataFrame(data.data, columns=feature_names)
    df['target'] = data.target
    return df, feature_names


def train_test_split_data(df: pd.DataFrame,
                          feature_names: list) -> tuple[pd.DataFrame, ...]:
    """
    Split the dataset into training and testing sets

    :param df: dataframe containing the dataset
    :param feature_names: list of feature names
    :return: lists containing X_train, X_test, y_train, y_test
    """
    X = df[feature_names]
    y = df['target']
    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.2,
                                                        random_state=42)

    return X_train, X_test, y_train, y_test


def train_model(X_train: pd.DataFrame,
                y_train: pd.DataFrame) -> RandomForestClassifier:
    """
    Train a Random Forest classifier model using the training data

    :param X_train: features of the training data
    :param y_train: target labels of the training data
    :return: trained Random Forest classifier model
    """
    model = RandomForestClassifier(n_estimators=100, random_state=42)
    model.fit(X_train.values, y_train.values)
    return model


def evaluate_model(model, X_test, y_test) -> None:
    """
    Evaluate the performance of the trained model using the testing data

    :param model: trained Random Forest classifier model
    :param X_test: features of the testing data
    :param y_test: target labels of the testing data
    :return: None
    """
    y_pred = model.predict(X_test.values)
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Accuracy: {accuracy}')


def make_prediction(model, features) -> None:
    """
    Make a prediction using the trained model and the input features

    :param model: trained Random Forest classifier model
    :param features: list of input features for prediction
    :return: None
    """
    prediction = model.predict([features])
    print(f'Predicted class: {prediction[0]}')


def user_input_features(feature_names: list) -> list:
    """
    Prompt the user to enter features of the iris
    and return a list of the input features.
    :return: list of input features for the iris
    """
    print("Enter features of the iris:")
    features = []
    for feature_name in feature_names:
        feature = float(input(f"{feature_name}: "))
        features.append(feature)
    return features


def main() -> None:
    """
    Load the Iris dataset, split it into training and testing sets,
    train a Random Forest classifier model, evaluate the model's performance,
    prompt the user to input features of the iris,
    and make a prediction using the trained model and the input features.

    :return: None
    """
    df, feature_names = load_data()

    X_train, X_test, y_train, y_test = train_test_split_data(df, feature_names)
    model = train_model(X_train, y_train)
    evaluate_model(model, X_test, y_test)

    # Request user input and make a prediction
    features = user_input_features(feature_names)
    make_prediction(model, features)


if __name__ == '__main__':
    main()
